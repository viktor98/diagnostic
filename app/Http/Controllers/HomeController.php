<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $chat = [
            [
                "user" => 1,
                'conversa'=>"Você sente dores no dia-a-dia?",
                'retorno' => "Dor no peito",
                'created_at' => "11:00"
            ],
            [
                "user" => 2,
                'conversa'=>"Sinto muito dor no peito as vezes.",
                'retorno' => "Dor no peito",
                'created_at' => "11:01"
            ],
            [
                "user" => 1,
                'conversa'=>"Você sente falta de ar ou fadiga?",
                'retorno' => "Dor no peito",
                'created_at' => "11:02"
            ],
            [
                "user" => 2,
                'conversa'=>"Sim, quando eu vou andar de manha para passear com o cachorro doi ao respirar, e depois de 5 minutos de caminhada me sinto cansado doutô. Também sinto canseira quando vou lavar a louça. Não é preguiça não, é canseira mesmo.",
                'retorno' => "Dor no peito",
                'created_at' => "11:03"
            ],
            [
                "user" => 1,
                'conversa'=>"Você já sentiu ou encontrou alguma irritação na pele?",
                'retorno' => "Dor no peito",
                'created_at' => "11:04"
            ],
            [
                "user" => 2,
                'conversa'=>"Meu cachorro mordeu meu braço e ficou vermelho.",
                'retorno' => "Dor no peito",
                'created_at' => "11:05"
            ],
            [
                "user" => 1,
                'conversa'=>"Desde quando você sente essas dores no peito?",
                'retorno' => "Dor no peito",
                'created_at' => "11:06"
            ],
            [
                "user" => 2,
                'conversa'=>"Acho que desde janeiro. Não não, fevereiro, eu senti na primeira vez depois de uma festa que fui e tive que sair por que estava com dificuldade de respirar.",
                'retorno' => "Dor no peito",
                'created_at' => "11:07"
            ],
            [
                "user" => 1,
                'conversa'=>"Eu vi aqui na triagem que você disse que estava sentindo enjoo. A quanto tempo esta sentindo enjoo?",
                'retorno' => "Dor no peito",
                'created_at' => "11:08"
            ],
            [
                "user" => 2,
                'conversa'=>"To vomitando que nem uma jaguara. Nada fica na minha barriga. Cansei de tomar sal de fruta e agua de coco.",
                'retorno' => "Dor no peito",
                'created_at' => "11:09"
            ],
            [
                "user" => 1,
                'conversa'=>"Você tem tosse?",
                'retorno' => "Dor no peito",
                'created_at' => "11:10"
            ],
            [
                "user" => 2,
                'conversa'=>"Fiquei gripado semana passada e tenho alergia a pó. Ah mas teve um dia que fui tomar banho e desmaiei e cai, bati a cabeça no vitro. Ta vendo o galo aqui. Minha mulher ate disse que eu tinha que vim no medico ou ia me largar.",
                'retorno' => "Dor no peito",
                'created_at' => "11:11"
            ],
            [
                "user" => 1,
                'conversa'=>"Você tem algum problema de sangue como diabete ou pressão alta?",
                'retorno' => "Dor no peito",
                'created_at' => "11:12"
            ],
            [
                "user" => 2,
                'conversa'=>"Acho que não. Eu tomo suco de cebola para prevenir pressão alta.",
                'retorno' => "Dor no peito",
                'created_at' => "11:13"
            ],
            [
                "user" => 1,
                'conversa'=>"Você toma quais tipos de remédio você toma?",
                'retorno' => "Dor no peito",
                'created_at' => "11:14"
            ],
            [
                "user" => 2,
                'conversa'=>"Tomo para dor nas juntas.",
                'retorno' => "Dor no peito",
                'created_at' => "11:15"
            ],
            [
                "user" => 1,
                'conversa'=>"Alguma dor no estomago ou azia?",
                'retorno' => "Dor no peito",
                'created_at' => "11:16"
            ],
            [
                "user" => 2,
                'conversa'=>"Azia quando tomo café, geralmente a noite antes de dormir ou quando acordo. Domingo também quando eu como um pastelzinho na feira.",
                'retorno' => "Dor no peito",
                'created_at' => "11:17"
            ],
            [
                "user" => 1,
                'conversa'=>"Quantas vezes por dia sente dores no peito?",
                'retorno' => "Dor no peito",
                'created_at' => "11:18"
            ],
            [
                "user" => 2,
                'conversa'=>"Não sei dizer, muitas vezes ao dia. Quando vou caminhando no mercado, padaria, esforço repentino né douto.",
                'retorno' => "Dor no peito",
                'created_at' => "11:19"
                
            ],
            [
                "user" => 1,
                'conversa'=>"Sente dor no coração?",
                'retorno' => "Dor no peito",
                'created_at' => "11:20"
            ],
            [
                "user" => 2,
                'conversa'=>"Sinto o coração acelerar de vez em quando, e umas pontadas no peito, principalmente depois do torresmo de sábado.",
                'retorno' => "Dor no peito",
                'created_at' => "11:21"
            ]

        ];

        return view('home', compact('chat'));
    }
}
