@extends('layouts.app')

@section('content')

    <div style="float:left;width:20%;height:1000px;overflow: auto; ">
        @foreach($chat as $c)
            @if($c['user'] == 2)
     
                <div class="chat">
                    <span class="user"> João (Paciente) </span>
                    <a href="#" class="pac" data-toggle="tooltip" title="{{$c['retorno']}}">
                        <p class="conversation">{{$c['conversa']}}</p>
                    </a>
                    <span class="time-right">{{$c['created_at']}}</span>
                </div>
         
            @else
                <div class="chat darker">
                    <span class="user right"> {{ Auth::user()->name }} (Médico) </span>
                    <p class="conversation">{{$c['conversa']}}</p>
                    <span class="time-left">{{$c['created_at']}}</span>
                </div>
            @endif
        @endforeach
    </div>
   

    @include('graph')
@endsection
